<?php
/**
 * @file
 * utct_misc_configuration_feature.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function utct_misc_configuration_feature_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'Create short URLs'.
  $permissions['Create short URLs'] = array(
    'name' => 'Create short URLs',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'shurly',
  );

  // Exported permission: 'Delete own URLs'.
  $permissions['Delete own URLs'] = array(
    'name' => 'Delete own URLs',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'shurly',
  );

  // Exported permission: 'Enter custom URLs'.
  $permissions['Enter custom URLs'] = array(
    'name' => 'Enter custom URLs',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'shurly',
  );

  // Exported permission: 'View own URL stats'.
  $permissions['View own URL stats'] = array(
    'name' => 'View own URL stats',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'shurly',
  );

  // Exported permission: 'access content'.
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
